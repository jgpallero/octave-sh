.. -*- coding: utf-8 -*-

.. This file is formatted using reStructuredText in order to be parsed into HTML
.. in https://bitbucket.org/jgpallero/libgeoc/

GNU Octave syntax highlighting files
====================================

This repository is intended as a warehouse to store syntax configuration files
for use GNU Octave language with different text editors.

For each language exists one folder that contains the syntax definition file and
an `install` file that contains the steps in order to use properly the syntax.
By the moment we have:

- ``codepad``: Syntax file for the MS Windows `CodePad` editor
  (http://shicola.wz.cz/codepad)
- ``kate``: Syntax file for the `Kate` and `KWrite` text editors from the KDE 3,
  KDE 4 and Plasma 5 environments.
- ``gedit``: Syntax file for the `gedit` text editor from Gnome environment.
- ``nano``: Syntax file for the `nano` editor and a `nanorc` configuration file.
- ``vim``: Syntax file for the `vim` editor and a `vimrc` configuration file.

The files (except the one for `gedit` contains all the function names provided
with GNU Octave 3.0.1 and Octave-Forge 20080216

Similar files can be seen in the GNU Octave wiki:
http://wiki.octave.org/wiki.pl?CodeEditorForOctave
