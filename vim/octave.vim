"octave.vim
"03-Nov-2011, José Luis García Pallero, <jgpallero@gmail.com>
" Vim syntax file
" Language:	octave
" Maintainer:	Jose Luis Garcia Pallero <jgpallero@gmail.com>
"       Last maintainer: Francisco Castro <fcr@adinet.com.uy>
"		Original maintainer: Preben 'Peppe' Guldberg <peppe-vim@wielders.org>
"		Original author: Mario Eusebio
"		Last changes: Se modifica la presentacion en pantalla de los números
"       enteros y reales. Se cambia el lenguaje a octave
"		Jose Luis Garcia Pallero <jgpallero@gmail.com>
" Last Change:	28 Apr 2008

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

function! s:CheckForFunctions()
    let l:i = 1
    let l:prev = ''
    while l:i <= line('$')
        let l:line = l:prev . getline(l:i)
        let l:i = l:i + 1
        if match(l:line, "\\(^\\|[^\\\\]\\)\\\\$") >= 0
            let l:prev = l:line[:-2]
            continue
        else
            let l:prev = ''
        endif
        if match(l:line, 'function') == 0
            let l:line = substitute(l:line,"function *\\(.*= *\\)\\?", "", "")
            let l:nfun = substitute(l:line,"\\([A-Za-z0-9_]\\+\\).*", "\\1", "")
            execute "syn keyword octaveFunction" l:nfun
        endif
    endwhile
endfunction

call s:CheckForFunctions()

" Palabras reservadas de GNU Octave
" 32 funciones
syn keyword octaveKeywords  break case catch continue do else elseif end
syn keyword octaveKeywords  end_try_catch end_unwind_protect endfor
syn keyword octaveKeywords  endfunction endif endswitch endwhile for function
syn keyword octaveKeywords  global if otherwise persistent replot return
syn keyword octaveKeywords  static switch try until unwind_protect
syn keyword octaveKeywords  unwind_protect_cleanup varargin varargout while

" Comandos de GNU Octave
" 41 funciones
syn keyword octaveCommands  __end__ addpath autoload cd chdir clear dbclear
syn keyword octaveCommands  dbstatus dbstop dbtype dbwhere diary echo
syn keyword octaveCommands  edit_history format help history iscommand
syn keyword octaveCommands  iskeyword israwcommand isvarname load lookfor
syn keyword octaveCommands  mark_as_command mark_as_rawcommand mislocked
syn keyword octaveCommands  mkdir mlock more munlock rmdir rmpath run_history
syn keyword octaveCommands  save type unmark_command unmark_rawcommand
syn keyword octaveCommands  warning which who whos

" Funciones por defecto de GNU Octave 3.0.1
" 1563 funciones
syn keyword octaveFunction  DEMOcontrol EDITOR EXEC_PATH F_DUPFD F_GETFD
syn keyword octaveFunction  F_GETFL F_SETFD F_SETFL I IMAGE_PATH Inf J NA NaN
syn keyword octaveFunction  OCTAVE_HOME OCTAVE_VERSION O_APPEND O_ASYNC
syn keyword octaveFunction  O_CREAT O_EXCL O_NONBLOCK O_RDONLY O_RDWR O_SYNC
syn keyword octaveFunction  O_TRUNC O_WRONLY PAGER PAGER_FLAGS PS1 PS2 PS4
syn keyword octaveFunction  P_tmpdir SEEK_CUR SEEK_END SEEK_SET SIG S_ISBLK
syn keyword octaveFunction  S_ISCHR S_ISDIR S_ISFIFO S_ISLNK S_ISREG S_ISSOCK
syn keyword octaveFunction  WCONTINUE WCOREDUMP WEXITSTATUS WIFCONTINUED
syn keyword octaveFunction  WIFEXITED WIFSIGNALED WIFSTOPPED WNOHANG WSTOPSIG
syn keyword octaveFunction  WTERMSIG WUNTRACED __abcddims__ __area__
syn keyword octaveFunction  __axes_limits__ __axis_label__ __bar__ __bars__
syn keyword octaveFunction  __bodquist__ __clear_plot_window__ __contour__
syn keyword octaveFunction  __contourc__ __default_plot_options__
syn keyword octaveFunction  __delaunayn__ __dsearchn__ __end__ __errcomm__
syn keyword octaveFunction  __error_text__ __errplot__ __freqresp__
syn keyword octaveFunction  __fsolve_defopts__ __glpk__ __gnuplot_plot__
syn keyword octaveFunction  __gnuplot_raw__ __gnuplot_replot__
syn keyword octaveFunction  __gnuplot_save_data__
syn keyword octaveFunction  __gnuplot_send_inline_data__ __gnuplot_set__
syn keyword octaveFunction  __gnuplot_show__ __gnuplot_splot__
syn keyword octaveFunction  __gnuplot_version__ __go_axes__ __go_axes_init__
syn keyword octaveFunction  __go_close_all__ __go_delete__ __go_draw_axes__
syn keyword octaveFunction  __go_draw_figure__ __go_figure__
syn keyword octaveFunction  __go_figure_handles__ __go_handles__ __go_image__
syn keyword octaveFunction  __go_line__ __go_patch__ __go_surface__
syn keyword octaveFunction  __go_text__ __gud_mode__ __img__ __img_via_file__
syn keyword octaveFunction  __isequal__ __lin_interpn__ __line__
syn keyword octaveFunction  __next_line_color__ __norm__ __outlist__
syn keyword octaveFunction  __patch__ __pchip_deriv__ __plr1__ __plr2__
syn keyword octaveFunction  __plt1__ __plt2__ __plt2mm__ __plt2mv__
syn keyword octaveFunction  __plt2ss__ __plt2sv__ __plt2vm__ __plt2vs__
syn keyword octaveFunction  __plt2vv__ __plt__ __plt_get_axis_arg__
syn keyword octaveFunction  __pltopt1__ __pltopt__ __print_symbol_info__
syn keyword octaveFunction  __print_symtab_info__ __qp__ __quiver__
syn keyword octaveFunction  __request_drawnow__ __scatter__ __splinen__
syn keyword octaveFunction  __stem__ __stepimp__ __syschnamesl__
syn keyword octaveFunction  __sysconcat__ __syscont_disc__ __sysdefioname__
syn keyword octaveFunction  __sysdefstname__ __sysgroupn__ __tf2sysl__
syn keyword octaveFunction  __tfl__ __token_count__ __version_info__
syn keyword octaveFunction  __voronoi__ __zgpbal__ __zp2ssg2__ abcddim abs
syn keyword octaveFunction  accumarray acos acosd acosh acot acotd acoth acsc
syn keyword octaveFunction  acscd acsch addpath airy all analdemo ancestor
syn keyword octaveFunction  and angle anova ans any append arch_fit arch_rnd
syn keyword octaveFunction  arch_test are area arg argnames argv arma_rnd
syn keyword octaveFunction  arrayfun asctime asec asecd asech asin asind
syn keyword octaveFunction  asinh assert assignin atan atan2 atand atanh
syn keyword octaveFunction  atexit autocor autocov autoload autoreg_matrix
syn keyword octaveFunction  autumn axes axis axis2dlim balance bar barh
syn keyword octaveFunction  bartlett bartlett_test base2dec bddemo beep
syn keyword octaveFunction  beep_on_error bessel besselh besseli besselj
syn keyword octaveFunction  besselk bessely beta beta_cdf beta_inv beta_pdf
syn keyword octaveFunction  beta_rnd betacdf betai betainc betainv betaln
syn keyword octaveFunction  betapdf betarnd bicubic bin2dec bincoeff binocdf
syn keyword octaveFunction  binoinv binomial_cdf binomial_inv binomial_pdf
syn keyword octaveFunction  binomial_rnd binopdf binornd bitand bitcmp bitget
syn keyword octaveFunction  bitmax bitor bitset bitshift bitxor blackman
syn keyword octaveFunction  blanks blkdiag bode bode_bounds bone box brighten
syn keyword octaveFunction  bsxfun bug_report buildssic builtin bunzip2 c2d
syn keyword octaveFunction  calendar canonicalize_file_name cart2pol cart2sph
syn keyword octaveFunction  cast cat cauchy_cdf cauchy_inv cauchy_pdf
syn keyword octaveFunction  cauchy_rnd caxis ccolamd cd ceil cell cell2mat
syn keyword octaveFunction  cell2struct celldisp cellfun cellidx cellstr
syn keyword octaveFunction  center char chdir chi2cdf chi2inv chi2pdf chi2rnd
syn keyword octaveFunction  chisquare_cdf chisquare_inv chisquare_pdf
syn keyword octaveFunction  chisquare_rnd chisquare_test_homogeneity
syn keyword octaveFunction  chisquare_test_independence chol chol2inv cholinv
syn keyword octaveFunction  circshift class clc clear clearplot clf clg clock
syn keyword octaveFunction  cloglog close closeplot closereq colamd colloc
syn keyword octaveFunction  colorbar colormap colperm columns com2str comma
syn keyword octaveFunction  common_size commutation_matrix compan
syn keyword octaveFunction  compare_versions compass complement
syn keyword octaveFunction  completion_append_char completion_matches complex
syn keyword octaveFunction  computer cond condest confirm_recursive_rmdir
syn keyword octaveFunction  conj contour contour3 contourc contourf
syn keyword octaveFunction  controldemo conv conv2 convhull convhulln cool
syn keyword octaveFunction  copper copyfile cor cor_test corrcoef cos cosd
syn keyword octaveFunction  cosh cot cotd coth cov cplxpair cputime
syn keyword octaveFunction  crash_dumps_octave_core create_set cross csc cscd
syn keyword octaveFunction  csch csymamd ctime ctranspose ctrb cumprod cumsum
syn keyword octaveFunction  cumtrapz cut cylinder d2c damp dare daspk
syn keyword octaveFunction  daspk_options dasrt dasrt_options dassl
syn keyword octaveFunction  dassl_options date datenum datestr datevec
syn keyword octaveFunction  dbclear dbstatus dbstop dbtype dbwhere dcgain
syn keyword octaveFunction  deal deblank debug_on_error debug_on_interrupt
syn keyword octaveFunction  debug_on_warning debug_symtab_lookups dec2base
syn keyword octaveFunction  dec2bin dec2hex deconv default_save_options del2
syn keyword octaveFunction  delaunay delaunay3 delaunayn delete demo demoquat
syn keyword octaveFunction  det detrend dezero dgkfdemo dgram dhinfdemo diag
syn keyword octaveFunction  diary diff diffpara dir discrete_cdf discrete_inv
syn keyword octaveFunction  discrete_pdf discrete_rnd disp dispatch dkalman
syn keyword octaveFunction  dlqe dlqg dlqr dlyap dmperm dmr2d dmult
syn keyword octaveFunction  do_string_escapes doc document dos dot double
syn keyword octaveFunction  drawnow dre dsearch dsearchn dump_prefs dup2
syn keyword octaveFunction  duplication_matrix durbinlevinson e echo
syn keyword octaveFunction  echo_executing_commands edit edit_history eig
syn keyword octaveFunction  ellipsoid empirical_cdf empirical_inv
syn keyword octaveFunction  empirical_pdf empirical_rnd endgrent endpwent
syn keyword octaveFunction  eomday eps eq erf erfc erfinv errno errno_list
syn keyword octaveFunction  error error_text errorbar etime etree etreeplot
syn keyword octaveFunction  eval evalin example exec exist exit exp expcdf
syn keyword octaveFunction  expinv expm exponential_cdf exponential_inv
syn keyword octaveFunction  exponential_pdf exponential_rnd exppdf exprnd eye
syn keyword octaveFunction  f_cdf f_inv f_pdf f_rnd f_test_regression factor
syn keyword octaveFunction  factorial fail false fcdf fclear fclose fcntl
syn keyword octaveFunction  fdisp feather feof ferror feval fflush fft fft2
syn keyword octaveFunction  fftconv fftfilt fftn fftshift fftw fgetl fgets
syn keyword octaveFunction  fieldnames figure file_in_loadpath file_in_path
syn keyword octaveFunction  fileattrib fileparts filesep fill filter filter2
syn keyword octaveFunction  find findobj findstr finite finv fir2sys fix
syn keyword octaveFunction  fixed_point_format flag flipdim fliplr flipud
syn keyword octaveFunction  floor flops fmod fnmatch fopen fork format
syn keyword octaveFunction  formula fpdf fplot fprintf fputs fractdiff frdemo
syn keyword octaveFunction  fread freport freqchkw freqz freqz_plot frewind
syn keyword octaveFunction  frnd fscanf fseek fsolve fsolve_options ftell
syn keyword octaveFunction  full fullfile func2str functions fv fvl fwrite
syn keyword octaveFunction  gamcdf gaminv gamma gamma_cdf gamma_inv gamma_pdf
syn keyword octaveFunction  gamma_rnd gammai gammainc gammaln gampdf gamrnd
syn keyword octaveFunction  gca gcd gcf ge genpath geocdf geoinv
syn keyword octaveFunction  geometric_cdf geometric_inv geometric_pdf
syn keyword octaveFunction  geometric_rnd geopdf geornd get getegid getenv
syn keyword octaveFunction  geteuid getfield getgid getgrent getgrgid
syn keyword octaveFunction  getgrnam getpgrp getpid getppid getpwent getpwnam
syn keyword octaveFunction  getpwuid getrusage getuid givens glob glpk
syn keyword octaveFunction  glpkmex gls gmap40 gmtime gnuplot_binary
syn keyword octaveFunction  gnuplot_command_axes gnuplot_command_end
syn keyword octaveFunction  gnuplot_command_plot gnuplot_command_replot
syn keyword octaveFunction  gnuplot_command_splot gnuplot_command_title
syn keyword octaveFunction  gnuplot_command_using gnuplot_command_with
syn keyword octaveFunction  gnuplot_use_title_option gplot gradient gram gray
syn keyword octaveFunction  gray2ind grid griddata griddata3 griddatan gt
syn keyword octaveFunction  gunzip gzip h2norm h2syn hadamard hamming hankel
syn keyword octaveFunction  hanning help hess hex2dec hidden hilb hinf_ctr
syn keyword octaveFunction  hinfdemo hinfnorm hinfsyn hinfsyn_chk hinfsyn_ric
syn keyword octaveFunction  hist history history_file history_size
syn keyword octaveFunction  history_timestamp_format_string hold home horzcat
syn keyword octaveFunction  hot hotelling_test hotelling_test_2 housh hsv
syn keyword octaveFunction  hsv2rgb hurst hygecdf hygeinv hygepdf hygernd
syn keyword octaveFunction  hypergeometric_cdf hypergeometric_inv
syn keyword octaveFunction  hypergeometric_pdf hypergeometric_rnd i ifft
syn keyword octaveFunction  ifft2 ifftn ifftshift ignore_function_time_stamp
syn keyword octaveFunction  imag image image_viewer imagesc impulse imshow
syn keyword octaveFunction  ind2gray ind2rgb ind2sub index inf info_file
syn keyword octaveFunction  info_program inline inpolygon input
syn keyword octaveFunction  input_event_hook inputname int16 int2str int32
syn keyword octaveFunction  int64 int8 interp1 interp2 interp3 interpft
syn keyword octaveFunction  interpn intersect intersection intmax intmin inv
syn keyword octaveFunction  inverse invhilb ipermute iqr irr is_abcd is_bool
syn keyword octaveFunction  is_complex is_controllable is_detectable is_dgkf
syn keyword octaveFunction  is_digital is_duplicate_entry is_global
syn keyword octaveFunction  is_leap_year is_list is_matrix is_observable
syn keyword octaveFunction  is_sample is_scalar is_signal_list is_siso
syn keyword octaveFunction  is_square is_stabilizable is_stable is_stream
syn keyword octaveFunction  is_struct is_symmetric is_vector isa isalnum
syn keyword octaveFunction  isalpha isascii isbool iscell iscellstr ischar
syn keyword octaveFunction  iscntrl iscommand iscomplex isdefinite isdigit
syn keyword octaveFunction  isdir isempty isequal isequalwithequalnans
syn keyword octaveFunction  isfield isfigure isfinite isglobal isgraph
syn keyword octaveFunction  ishandle ishold isieee isinf isinteger iskeyword
syn keyword octaveFunction  isletter islist islogical islower ismac ismatrix
syn keyword octaveFunction  ismember isna isnan isnumeric ispc isprime
syn keyword octaveFunction  isprint ispunct israwcommand isreal isscalar
syn keyword octaveFunction  isspace issparse issquare isstr isstreamoff
syn keyword octaveFunction  isstruct issymmetric isunix isupper isvarname
syn keyword octaveFunction  isvector isxdigit j jet jet707 kbhit kendall
syn keyword octaveFunction  keyboard kill kolmogorov_smirnov_cdf
syn keyword octaveFunction  kolmogorov_smirnov_test kolmogorov_smirnov_test_2
syn keyword octaveFunction  kron kruskal_wallis_test krylov krylovb kurtosis
syn keyword octaveFunction  laplace_cdf laplace_inv laplace_pdf laplace_rnd
syn keyword octaveFunction  lasterr lasterror lastwarn lchol lcm ldivide le
syn keyword octaveFunction  legend legendre length lgamma license lin2mu line
syn keyword octaveFunction  link linspace list list_in_columns list_primes
syn keyword octaveFunction  listidx load loadaudio loadimage localtime log
syn keyword octaveFunction  log10 log2 logical logistic_cdf logistic_inv
syn keyword octaveFunction  logistic_pdf logistic_regression
syn keyword octaveFunction  logistic_regression_derivatives
syn keyword octaveFunction  logistic_regression_likelihood logistic_rnd logit
syn keyword octaveFunction  loglog loglogerr logm logncdf logninv
syn keyword octaveFunction  lognormal_cdf lognormal_inv lognormal_pdf
syn keyword octaveFunction  lognormal_rnd lognpdf lognrnd logspace lookfor
syn keyword octaveFunction  lookup lower lqe lqg lqr ls ls_command lsim lsode
syn keyword octaveFunction  lsode_options lstat lt ltifr lu luinc lyap magic
syn keyword octaveFunction  mahalanobis makeinfo_program manova
syn keyword octaveFunction  mark_as_command mark_as_rawcommand mat2cell
syn keyword octaveFunction  mat2str matrix_type max max_recursion_depth
syn keyword octaveFunction  mcnemar_test md5sum mean meansq median menu mesh
syn keyword octaveFunction  meshc meshdom meshgrid meshz mex mexext mfilename
syn keyword octaveFunction  min minfo minus mislocked mkdir mkfifo mkoctfile
syn keyword octaveFunction  mkpp mkstemp mktime mldivide mlock mod moddemo
syn keyword octaveFunction  mode moment more movefile mpoles mpower mrdivide
syn keyword octaveFunction  mtimes mu2lin munlock nan nargchk nargin nargout
syn keyword octaveFunction  native_float_format nbincdf nbininv nbinpdf
syn keyword octaveFunction  nbinrnd nchoosek ndgrid ndims ne newplot news
syn keyword octaveFunction  nextpow2 nichols nnz nonzeros norm normal_cdf
syn keyword octaveFunction  normal_inv normal_pdf normal_rnd normcdf normest
syn keyword octaveFunction  norminv normpdf normrnd not now nper npv nth
syn keyword octaveFunction  nthroot ntsc2rgb null num2cell num2str numel
syn keyword octaveFunction  nyquist nzmax obsv ocean octave_config_info
syn keyword octaveFunction  octave_core_file_limit octave_core_file_name
syn keyword octaveFunction  octave_core_file_options octave_tmp_file_name ols
syn keyword octaveFunction  onenormest ones optimset or ord2 orderfields
syn keyword octaveFunction  orient orth output_max_field_width
syn keyword octaveFunction  output_precision pack packedform packsys
syn keyword octaveFunction  page_output_immediately page_screen_output
syn keyword octaveFunction  parallel paren pareto parseparams pascal
syn keyword octaveFunction  pascal_cdf pascal_inv pascal_pdf pascal_rnd patch
syn keyword octaveFunction  path pathdef pathsep pause pcg pchip pclose
syn keyword octaveFunction  pcolor pcr peaks periodogram perms permute perror
syn keyword octaveFunction  pi pie pink pinv pipe pkg place playaudio plot
syn keyword octaveFunction  plot3 plotyy plus pmt poisscdf poissinv
syn keyword octaveFunction  poisson_cdf poisson_inv poisson_pdf poisson_rnd
syn keyword octaveFunction  poisspdf poissrnd pol2cart polar poly polyarea
syn keyword octaveFunction  polyder polyderiv polyfit polygcd polyint
syn keyword octaveFunction  polyinteg polyout polyreduce polyval polyvalm
syn keyword octaveFunction  popen popen2 postpad pow2 power ppplot ppval
syn keyword octaveFunction  prepad primes print print_answer_id_name
syn keyword octaveFunction  print_empty_dimensions print_usage printf prism
syn keyword octaveFunction  probit prod program_invocation_name program_name
syn keyword octaveFunction  prompt prop_test_2 purge_tmp_files putenv puts pv
syn keyword octaveFunction  pvl pwd pzmap qconj qcoordinate_plot qderiv
syn keyword octaveFunction  qderivmat qinv qmult qp qqplot qr qtrans qtransv
syn keyword octaveFunction  qtransvmat quad quad_options quadl quaternion
syn keyword octaveFunction  quit quiver quiver3 qz qzhess qzval rainbow rand
syn keyword octaveFunction  rande randg randn randp randperm range rank ranks
syn keyword octaveFunction  rat rate rats rdivide read_readline_init_file
syn keyword octaveFunction  readdir readlink real realmax realmin record
syn keyword octaveFunction  rectangle_lw rectangle_sw regexp regexpi
syn keyword octaveFunction  regexprep rehash rem rename replot repmat reshape
syn keyword octaveFunction  residue resize rethrow reverse rgb2hsv rgb2ind
syn keyword octaveFunction  rgb2ntsc ribbon rindex rldemo rlocus rmdir
syn keyword octaveFunction  rmfield rmpath roots rose rosser rot90 rotdim
syn keyword octaveFunction  rotg round rows rref run run_cmd run_count
syn keyword octaveFunction  run_history run_test runlength save
syn keyword octaveFunction  save_header_format_string save_precision
syn keyword octaveFunction  saveaudio saveimage savepath saving_history scanf
syn keyword octaveFunction  scatter scatter3 schur sec secd sech semicolon
syn keyword octaveFunction  semilogx semilogxerr semilogy semilogyerr series
syn keyword octaveFunction  set setaudio setdiff setfield setgrent setpwent
syn keyword octaveFunction  setstr setxor shading shell_cmd shg shift
syn keyword octaveFunction  shiftdim sighup_dumps_octave_core sign sign_test
syn keyword octaveFunction  sigterm_dumps_octave_core silent_functions sin
syn keyword octaveFunction  sinc sind sinetone sinewave single sinh size
syn keyword octaveFunction  size_equal sizeof skewness sleep slice sombrero
syn keyword octaveFunction  sort sortcom sortrows source spalloc sparse
syn keyword octaveFunction  sparse_auto_mutate spatan2 spchol spchol2inv
syn keyword octaveFunction  spcholinv spconvert spcumprod spcumsum spdet
syn keyword octaveFunction  spdiag spdiags spearman spectral_adf spectral_xdf
syn keyword octaveFunction  speed spencer speye spfind spfun sph2cart sphcat
syn keyword octaveFunction  sphere spinmap spinv spkron splchol splice spline
syn keyword octaveFunction  split split_long_rows splu spmax spmin spones
syn keyword octaveFunction  spparms spprod spqr sprand sprandn sprandsym
syn keyword octaveFunction  sprank spring sprintf spstats spsum spsumsq
syn keyword octaveFunction  spvcat spy sqp sqrt sqrtm squeeze ss ss2sys ss2tf
syn keyword octaveFunction  ss2zp sscanf stairs starp stat statistics std
syn keyword octaveFunction  stderr stdin stdnormal_cdf stdnormal_inv
syn keyword octaveFunction  stdnormal_pdf stdnormal_rnd stdout stem stem3
syn keyword octaveFunction  step stft str2double str2func str2mat str2num
syn keyword octaveFunction  strappend strcat strcmp strcmpi streamoff
syn keyword octaveFunction  strerror strfind strftime string_fill_char
syn keyword octaveFunction  strjust strmatch strncmp strncmpi strptime strrep
syn keyword octaveFunction  strtok strtrim strtrunc struct struct2cell
syn keyword octaveFunction  struct_contains struct_elements
syn keyword octaveFunction  struct_levels_to_print structfun strvcat
syn keyword octaveFunction  studentize sub2ind subplot subsasgn subsref
syn keyword octaveFunction  substr substruct sum summer sumsq
syn keyword octaveFunction  suppress_verbose_help_message surf surface surfc
syn keyword octaveFunction  surfnorm svd swap swapbytes swapcols swaprows syl
syn keyword octaveFunction  sylvester_matrix symamd symbfact symlink symrcm
syn keyword octaveFunction  synthesis sys2fir sys2ss sys2tf sys2zp sysadd
syn keyword octaveFunction  sysappend syschnames syschtsam sysconnect syscont
syn keyword octaveFunction  sysdimensions sysdisc sysdup sysgetsignals
syn keyword octaveFunction  sysgettsam sysgettype sysgroup sysidx sysmin
syn keyword octaveFunction  sysmult sysout sysprune sysreorder sysrepdemo
syn keyword octaveFunction  sysscale syssetsignals syssub system sysupdate
syn keyword octaveFunction  t_cdf t_inv t_pdf t_rnd t_test t_test_2
syn keyword octaveFunction  t_test_regression table tan tand tanh tar tcdf
syn keyword octaveFunction  tempdir tempname terminal_size test texas_lotto
syn keyword octaveFunction  text tf tf2ss tf2sys tf2zp tfout tic tilde_expand
syn keyword octaveFunction  time times tinv title tmpfile tmpnam toascii toc
syn keyword octaveFunction  toeplitz tolower toupper tpdf trace transpose
syn keyword octaveFunction  trapz treeplot triangle_lw triangle_sw tril
syn keyword octaveFunction  trimesh triplot triu trnd true tsearch tsearchn
syn keyword octaveFunction  type typecast typeinfo tzero tzero2 u_test ugain
syn keyword octaveFunction  uint16 uint32 uint64 uint8 umask uminus uname
syn keyword octaveFunction  undo_string_escapes unidcdf unidinv unidpdf
syn keyword octaveFunction  unidrnd unifcdf unifinv uniform_cdf uniform_inv
syn keyword octaveFunction  uniform_pdf uniform_rnd unifpdf unifrnd union
syn keyword octaveFunction  unique unix unlink unmark_command
syn keyword octaveFunction  unmark_rawcommand unmkpp unpack unpacksys untar
syn keyword octaveFunction  unwrap unzip uplus upper urlread urlwrite usage
syn keyword octaveFunction  usleep values vander var var_test
syn keyword octaveFunction  variables_can_hide_functions vec vech vectorize
syn keyword octaveFunction  ver version vertcat view vol voronoi voronoin
syn keyword octaveFunction  waitpid warning warning_ids warranty wavread
syn keyword octaveFunction  wavwrite wblcdf wblinv wblpdf wblrnd weekday
syn keyword octaveFunction  weibcdf weibinv weibpdf weibrnd weibull_cdf
syn keyword octaveFunction  weibull_inv weibull_pdf weibull_rnd welch_test
syn keyword octaveFunction  wgt1o what which white who whos whos_line_format
syn keyword octaveFunction  wiener_rnd wienrnd wilcoxon_test wilkinson winter
syn keyword octaveFunction  xlabel xlim xor yes_or_no ylabel ylim yulewalker
syn keyword octaveFunction  z_test z_test_2 zeros zgfmul zgfslv zginit
syn keyword octaveFunction  zgreduce zgrownorm zgscal zgsgiv zgshsr zip
syn keyword octaveFunction  zlabel zlim zp zp2ss zp2sys zp2tf zpout

" Funciones de octave-forge 20080216
" 1401 funciones
syn keyword octaveForge  AF_INET AF_LOCAL AF_UNIX AtomicWeight BIM2Aadvdiff
syn keyword octaveForge  BIM2Aboundarymass BIM2Areaction BIM2Arhs
syn keyword octaveForge  BIM2Cglobalflux BIM2Cmeshproperties BIM2Cpdegrad
syn keyword octaveForge  BIM2Cunknowncoord BIM2Cunknownsonside BIMUbern
syn keyword octaveForge  BIMUlogm BandToFull BandToSparse CS_Compt
syn keyword octaveForge  CS_FluorLine CS_KN CS_Photo CS_Rayl CS_Total
syn keyword octaveForge  CSb_Compt CSb_FluorLine CSb_Photo CSb_Rayl CSb_Total
syn keyword octaveForge  Chi Ci ComptonEnergy Contents Cos CosKronTransProb
syn keyword octaveForge  Cosh D DCSP_Compt DCSP_KN DCSP_Rayl DCSP_Thoms
syn keyword octaveForge  DCSPb_Compt DCSPb_Rayl DCS_Compt DCS_KN DCS_Rayl
syn keyword octaveForge  DCS_Thoms DCSb_Compt DCSb_Rayl
syn keyword octaveForge  DDGOXTelectron_driftdiffusion DDGOXTgummelmap
syn keyword octaveForge  DDGOXThole_driftdiffusion DDGOXddcurrent
syn keyword octaveForge  DDGOXelectron_driftdiffusion DDGOXgummelmap
syn keyword octaveForge  DDGOXhole_driftdiffusion DDGOXnlpoisson
syn keyword octaveForge  DDGOXplotresults DDGelectron_driftdiffusion
syn keyword octaveForge  DDGgummelmap DDGhole_driftdiffusion DDGn2phin
syn keyword octaveForge  DDGnlpoisson DDGp2phip DDGphin2n DDGphip2p
syn keyword octaveForge  DDGplotresults DDNnewtonmap EdgeEnergy
syn keyword octaveForge  ExampleEigenValues ExampleGenEigenValues Exp FF_Rayl
syn keyword octaveForge  FPL2dxappenddata FPL2dxoutputdata
syn keyword octaveForge  FPL2dxoutputtimeseries FPL2pdequiver FPL2pdeshowmesh
syn keyword octaveForge  FPL2pdesurf FPL2ptcquiver FPL2ptcshowmesh
syn keyword octaveForge  FPL2ptcsurf FPL2trspdesurf FPL2trsptcsurf FluorYield
syn keyword octaveForge  FullToBand GramSchmidt JumpFactor LineEnergy
syn keyword octaveForge  LinearRegression Log METLINEScapcomp
syn keyword octaveForge  METLINESdefinepermittivity
syn keyword octaveForge  MSH2Mdisplacementsmoothing MSH2Mgeomprop MSH2Mgmsh
syn keyword octaveForge  MSH2Mjoinstructm MSH2Mmeshalongspline
syn keyword octaveForge  MSH2Mnodesonsides MSH2Mstructmesh MSH2Msubmesh
syn keyword octaveForge  MSH2Mtopprop MomentTransf Pi QDDGOXcompdens
syn keyword octaveForge  QDDGOXddcurrent QDDGOXgummelmap QDDGOXnlpoisson
syn keyword octaveForge  RadRate SBBacksub SBEig SBFactor SBProd SBSolve
syn keyword octaveForge  SF_Compt SHA1 SOCK_DGRAM SOCK_RAW SOCK_RDM
syn keyword octaveForge  SOCK_SEQPACKET SOCK_STREAM Shi Si Sin Sinh Sqrt
syn keyword octaveForge  SymBand Tan Tanh ThDDGOXMOBN0STD ThDDGOXMOBN1STD
syn keyword octaveForge  ThDDGOXMOBP0STD ThDDGOXMOBP1STD ThDDGOXTWN0STD
syn keyword octaveForge  ThDDGOXTWN1STD ThDDGOXTWP0STD ThDDGOXTWP1STD
syn keyword octaveForge  ThDDGOXddcurrent ThDDGOXelectron_driftdiffusion
syn keyword octaveForge  ThDDGOXeletiteration ThDDGOXgummelmap
syn keyword octaveForge  ThDDGOXhole_driftdiffusion ThDDGOXnlpoisson
syn keyword octaveForge  ThDDGOXthermaliteration ThDDGOXupdateelectron_temp
syn keyword octaveForge  ThDDGOXupdatehole_temp ThDDGOXupdatelattice_temp
syn keyword octaveForge  UDXappend2Ddata UDXoutput2Ddata
syn keyword octaveForge  UDXoutput2Dtimeseries URREcyclingpattern Ubern
syn keyword octaveForge  Ubernoulli Ucolumns Ucompconst Ucomplap Ucompmass
syn keyword octaveForge  Ucompmass2 Udescaling Udopdepmob Udrawedge
syn keyword octaveForge  Udriftdepmob Udriftdiffusion Udriftdiffusion2
syn keyword octaveForge  Ufielddepmob Ufvsgcurrent Ufvsgcurrent2
syn keyword octaveForge  Ufvsgcurrent3 Uinvfermidirac Uise2pde Ujoinmeshes
syn keyword octaveForge  Umediaarmonica Umeshproperties Umsh2pdetool
syn keyword octaveForge  Umshcreatemesh Unodesonside Updegrad Updemesh
syn keyword octaveForge  Updesurf Urows Urrextrapolation Uscaling
syn keyword octaveForge  Uscharfettergummel Uscharfettergummel2
syn keyword octaveForge  Uscharfettergummel3 Usmoothguess Ustructmesh
syn keyword octaveForge  Ustructmesh_left Ustructmesh_random
syn keyword octaveForge  Ustructmesh_right Usubdomains2 Usubmesh Utemplogm
syn keyword octaveForge  __bfgsmin __bwdist __calcjacobian __calcperf
syn keyword octaveForge  __checknetstruct __dlogsig __dpurelin __dtansig
syn keyword octaveForge  __ellip_ws __ellip_ws_min __errcore__ __ga__ __getx
syn keyword octaveForge  __gfweight__ __grcla__ __grclf__ __grcmd__
syn keyword octaveForge  __grexit__ __grfigure__ __grgetstat__ __grhold__
syn keyword octaveForge  __grinit__ __grishold__ __grnewset__ __grsetgraph__
syn keyword octaveForge  __init __kernel_epanechnikov __kernel_normal
syn keyword octaveForge  __kernel_weights __mse __newnetwork
syn keyword octaveForge  __nlnewmark_fcn__ __power __printAdaptFcn
syn keyword octaveForge  __printAdaptParam __printB __printBiasConnect
syn keyword octaveForge  __printBiases __printIW __printInitFcn
syn keyword octaveForge  __printInitParam __printInputConnect
syn keyword octaveForge  __printInputWeights __printInputs __printLW
syn keyword octaveForge  __printLayerConnect __printLayerWeights
syn keyword octaveForge  __printLayers __printMLPHeader __printNetworkType
syn keyword octaveForge  __printNumInputDelays __printNumInputs
syn keyword octaveForge  __printNumLayerDelays __printNumLayers
syn keyword octaveForge  __printNumOutputs __printNumTargets
syn keyword octaveForge  __printOutputConnect __printOutputs
syn keyword octaveForge  __printPerformFcn __printPerformParam
syn keyword octaveForge  __printTargetConnect __printTargets __printTrainFcn
syn keyword octaveForge  __printTrainParam __setx __trainlm aCos aCosh aSin
syn keyword octaveForge  aSinh aTan aTanh aar aarmam ac2poly ac2rc accept
syn keyword octaveForge  acorf acovf actxserver ademodce adim adsmax airy_Ai
syn keyword octaveForge  airy_Ai_deriv airy_Ai_deriv_scaled airy_Ai_scaled
syn keyword octaveForge  airy_Bi airy_Bi_deriv airy_Bi_deriv_scaled
syn keyword octaveForge  airy_Bi_scaled airy_zero_Ai airy_zero_Ai_deriv
syn keyword octaveForge  airy_zero_Bi airy_zero_Bi_deriv ajuda amarma amdemod
syn keyword octaveForge  ammod amodce anderson_darling_cdf
syn keyword octaveForge  anderson_darling_test anovan apkconst append_save
syn keyword octaveForge  apply applylut ar2poly ar2rc ar_psd ar_spa arburg
syn keyword octaveForge  arcext arfit2 arithmetic_decode arithmetic_encode
syn keyword octaveForge  aryule atanint att au auload auplot aurecord ausave
syn keyword octaveForge  autonan autoscale average_moments awgn azimuth
syn keyword octaveForge  barthannwin base64decode base64encode battery
syn keyword octaveForge  bchdeco bchenco bchpoly bessel_In bessel_In_scaled
syn keyword octaveForge  bessel_Inu bessel_Inu_scaled bessel_Jn bessel_Jnu
syn keyword octaveForge  bessel_Kn bessel_Kn_scaled bessel_Knu
syn keyword octaveForge  bessel_Knu_scaled bessel_Yn bessel_Ynu
syn keyword octaveForge  bessel_il_scaled bessel_jl bessel_kl_scaled
syn keyword octaveForge  bessel_lnKnu bessel_yl bessel_zero_J0 bessel_zero_J1
syn keyword octaveForge  best_dir best_dir_cov bestblk beta_gsl betastat
syn keyword octaveForge  bfgsmin bfgsmin_example bi2de biacovf bicg bilinear
syn keyword octaveForge  bind binostat bisdemo bispec biterr bitrevorder
syn keyword octaveForge  blackmanharris blackmannuttall blkproc bmpwrite
syn keyword octaveForge  bohmanwin bound_convex boxcar boxplot bsc bscchannel
syn keyword octaveForge  busdate butter buttord bwarea bwborder bwdist
syn keyword octaveForge  bweuler bwfill bwlabel bwmorph bwperim bwselect
syn keyword octaveForge  cceps cdiff cell2csv center cheb cheb1ord cheb2ord
syn keyword octaveForge  chebwin cheby1 cheby2 chi2stat chirp chisqouttest
syn keyword octaveForge  clausen clip cmpermute cmunique cochrancdf
syn keyword octaveForge  cochraninv cochrantest coeff
syn keyword octaveForge  coefficient_of_variation cohere col2im colfilt
syn keyword octaveForge  collect colorgradient com_atexit com_delete com_get
syn keyword octaveForge  com_invoke com_release com_set combs comms compand
syn keyword octaveForge  condeig condentr_seq conditionalentropy_XY
syn keyword octaveForge  conditionalentropy_YX conicalP_0 conicalP_1
syn keyword octaveForge  conicalP_half conicalP_mhalf conndef connect
syn keyword octaveForge  constants content contents conv2nan convmtx cor
syn keyword octaveForge  cordflt2 corr2 corrcoef cosets cosint count
syn keyword octaveForge  coupling_3j coupling_6j coupling_9j cov covm
syn keyword octaveForge  cplxreal cpsd cquadnd creadpdb crule crule2d
syn keyword octaveForge  crule2dgen csape csapi csaps csd cstrcmp csv2cell
syn keyword octaveForge  csv2latex csvconcat csvexplode csvread csvwrite
syn keyword octaveForge  ctranspose cyclgen cyclpoly czt d2_min datatype
syn keyword octaveForge  dateaxis datefind datesplit dawson day daysact dct
syn keyword octaveForge  dct2 dctmtx de2bi debye_1 debye_2 debye_3 debye_4
syn keyword octaveForge  decimate decode deg2rad degree deintrlv
syn keyword octaveForge  deletewithsemaphores delta_method demo_octgpr
syn keyword octaveForge  demodmap deref deriche deriv detrend dfdp dftmtx
syn keyword octaveForge  differentiate digits dilate dim dirac diric
syn keyword octaveForge  disconnect distance dixoncdf dixoninv dixontest
syn keyword octaveForge  dldsolver dlmread dlmwrite downsample dst durlev
syn keyword octaveForge  dxfwrite easter edge editdistance egolaydec
syn keyword octaveForge  egolayenc egolaygen eigs ellint_Ecomp ellint_Kcomp
syn keyword octaveForge  ellip ellipdemo ellipj ellipke ellipord encode endef
syn keyword octaveForge  entropy eomdate erf_Q erf_Z erf_gsl erfc_gsl erfcinv
syn keyword octaveForge  erfcx erode eta eta_int ex_matrix example_netcdf
syn keyword octaveForge  example_opendap example_optiminterp existfile
syn keyword octaveForge  exp_mult expand expdemo expfit expint expint_3
syn keyword octaveForge  expint_E1 expint_E2 expint_Ei expm1 exprel exprel_2
syn keyword octaveForge  exprel_n expstat eyediagram fbusdate fcnchk feedback
syn keyword octaveForge  fermi_dirac_3half fermi_dirac_half fermi_dirac_inc_0
syn keyword octaveForge  fermi_dirac_int fermi_dirac_mhalf ff2n fftconv2
syn keyword octaveForge  fibodeco fiboenco fibosplitstream filtfilt filtic
syn keyword octaveForge  findfiles findsym findsymbols fir1 fir2 firls
syn keyword octaveForge  flag_implicit_samplerate flag_implicit_significance
syn keyword octaveForge  flattopwin flix fmdemod fmin fminbnd fmins
syn keyword octaveForge  fminsearch fminunc fmmod fnder fnplt fnval freqs
syn keyword octaveForge  freqs_plot fspecial fstat fullfact funm fxpow fzero
syn keyword octaveForge  gamfit gamlike gamma_gsl gamma_inc gamma_inc_P
syn keyword octaveForge  gamma_inc_Q gammainv_gsl gammastar gamstat gapTest
syn keyword octaveForge  gauspuls gaussian gausswin gconv gconvmtx gcvspl
syn keyword octaveForge  gdeconv gdet gdftmtx gdiag gen2par genqamdemod
syn keyword octaveForge  genqammod geomean geostat get_graph_data getfields
syn keyword octaveForge  gethostbyname gethostname getid getusername gexp gf
syn keyword octaveForge  gfft gfilter gftable gfweight gget gifft ginput ginv
syn keyword octaveForge  ginverse gisequal glimh glimh2 gliml glog glu
syn keyword octaveForge  gmm_estimate gmm_example gmm_obj gmm_results
syn keyword octaveForge  gmm_variance gmm_variance_inefficient gmonopuls
syn keyword octaveForge  golombdeco golombenco gpr_predict gpr_train gprod
syn keyword octaveForge  gquad gquad2d gquad2d6 gquad2dgen gquad6 gquadnd
syn keyword octaveForge  gradabs gradacos gradacosh gradasin gradasinh
syn keyword octaveForge  gradatan gradatanh gradconj gradcos gradcosh gradcot
syn keyword octaveForge  gradcumprod gradcumsum gradexp gradfind gradimag
syn keyword octaveForge  gradinit gradlog gradlog10 gradprod gradreal gradsin
syn keyword octaveForge  gradsinh gradsqrt gradsum gradtan gradtanh grank
syn keyword octaveForge  graycomatrix grayslice graythresh grep greshape
syn keyword octaveForge  groots grpdelay grubbscdf grubbsinv grubbstest grule
syn keyword octaveForge  grule2d grule2dgen gsl_sf gsqrt gsum gsumsq gsvd gxy
syn keyword octaveForge  gxy1 gxy2 hammgen hann harmmean hartley_entropy
syn keyword octaveForge  hazard heaviside highlow hilbert hist2d histeq
syn keyword octaveForge  histfit histo histo2 histo3 histo4 hmmestimate
syn keyword octaveForge  hmmgenerate hmmviterbi holidays houghtf hour
syn keyword octaveForge  huffmandeco huffmandict huffmanenco hup hx hygestat
syn keyword octaveForge  hyperg_0F1 hzeta idct idct2 idplot idsim idst im2bw
syn keyword octaveForge  im2col im2double im2uint16 im2uint8 imadjust
syn keyword octaveForge  imfilter imginfo imhist imnoise impad
syn keyword octaveForge  imperspectivewarp impz imread imremap imresize
syn keyword octaveForge  imrotate imrotate_Fourier imshear imsmooth
syn keyword octaveForge  imtranslate imwrite infoentr_seq infogain_seq
syn keyword octaveForge  infoskeleton innerfun interp intrlv invest0 invest1
syn keyword octaveForge  invfdemo invfreq invfreqs invfreqz invoke inz
syn keyword octaveForge  irsa_act irsa_actcore irsa_check irsa_dft irsa_dftfp
syn keyword octaveForge  irsa_genreal irsa_idft irsa_isregular irsa_jitsp
syn keyword octaveForge  irsa_mdsp irsa_normalize irsa_plotdft irsa_resample
syn keyword octaveForge  irsa_rgenreal is_ex is_sym is_vpa isbusday isbw
syn keyword octaveForge  isgalois isgradient isgray isind isposint
syn keyword octaveForge  isprimitive isrecord isrgb issorted istri java2mat
syn keyword octaveForge  javaArray java_convert_matrix java_debug java_exit
syn keyword octaveForge  java_get java_init java_invoke java_new java_set
syn keyword octaveForge  java_unsigned_conversion javaaddpath javaclasspath
syn keyword octaveForge  jointentropy jpgread jpgwrite jsucdf jsupdf kaiser
syn keyword octaveForge  kaiserord kernel_density kernel_density_cvscore
syn keyword octaveForge  kernel_density_nodes kernel_example
syn keyword octaveForge  kernel_optimal_bandwidth kernel_regression
syn keyword octaveForge  kernel_regression_cvscore kernel_regression_nodes
syn keyword octaveForge  kullback_leibler_distance kurtosis label2rgb
syn keyword octaveForge  lambert_W0 lambert_Wm1 lambertw lattice lauchli
syn keyword octaveForge  laverage lbusdate lcoeff lcrcl lcrcu ldegree leasqr
syn keyword octaveForge  leasqrdemo legendre_Pl legendre_Plm legendre_Ql
syn keyword octaveForge  legendre_sphPlm legendre_sphPlm_array leval levinson
syn keyword octaveForge  lfdif line_min linkage listen lloyds lnbeta lncosh
syn keyword octaveForge  lngamma_gsl lnpoch lnsinh log_1plusx log_1plusx_mx
syn keyword octaveForge  log_erfc lognstat logsig lowertri lpc lweekdate
syn keyword octaveForge  lz77deco lz77enco m2xdate mad makelut map marcumq
syn keyword octaveForge  marginalc marginalr mark_for_deletion mat2gray match
syn keyword octaveForge  matdeintrlv matintrlv mdsmax mean mean2 meandev
syn keyword octaveForge  meansq medfilt1 medfilt2 median mexihat meyeraux
syn keyword octaveForge  min_max minimize minpol minute mktheta mle_estimate
syn keyword octaveForge  mle_example mle_obj mle_obj_nodes mle_results
syn keyword octaveForge  mle_variance mod modmap moment month months morlet
syn keyword octaveForge  movavg mscohere multicore multicoredemo
syn keyword octaveForge  multicoredemo_octave mutualinfo_seq
syn keyword octaveForge  mutualinformation mvaar mvar mvfilter mvfreqz mvnpdf
syn keyword octaveForge  mvnrnd name naninsttest nanmax nanmean nanmedian
syn keyword octaveForge  nanmin nanstd nansum nantest nanvar narysource
syn keyword octaveForge  nbinstat ncatt ncauer ncautonan ncautoscale ncbyte
syn keyword octaveForge  ncchar ncclose ncdatatype ncdim ncdouble ncdump
syn keyword octaveForge  ncenddef ncfloat ncint ncisrecord nclong ncname
syn keyword octaveForge  ncredef ncrule ncshort ncsync nctest ncvar
syn keyword octaveForge  nelder_mead_min netcdf newff newmark nlfilter
syn keyword octaveForge  nlnewmark nls_estimate nls_example nls_obj
syn keyword octaveForge  nls_obj_nodes nmsmax normcdf norminv normpdf
syn keyword octaveForge  normplot normstat nrm numden numgradient numhessian
syn keyword octaveForge  nuttallwin nweekdate nze oct2dec ode23 ode45 ode54
syn keyword octaveForge  ode78 odeget odephas2 odephas3 odepkg
syn keyword octaveForge  odepkg_equations_ilorenz odepkg_equations_lorenz
syn keyword octaveForge  odepkg_equations_pendulous odepkg_equations_roessler
syn keyword octaveForge  odepkg_equations_secondorderlag
syn keyword octaveForge  odepkg_equations_vanderpol odepkg_event_handle
syn keyword octaveForge  odepkg_structure_check odepkg_testsuite_calcmescd
syn keyword octaveForge  odepkg_testsuite_calcscd odepkg_testsuite_chemakzo
syn keyword octaveForge  odepkg_testsuite_hires odepkg_testsuite_implakzo
syn keyword octaveForge  odepkg_testsuite_implrober
syn keyword octaveForge  odepkg_testsuite_oregonator
syn keyword octaveForge  odepkg_testsuite_pollution
syn keyword octaveForge  odepkg_testsuite_robertson
syn keyword octaveForge  odepkg_testsuite_transistor odeplot odeprint odeset
syn keyword octaveForge  optiminterp optiminterp1 optiminterp2 optiminterp3
syn keyword octaveForge  optiminterp4 optimset ordfilt2 outer outlier pacf
syn keyword octaveForge  padarray pamdemod pammod parameterize parcor partcnt
syn keyword octaveForge  partint parzenwin pburg pcregexp pdist percentile
syn keyword octaveForge  physical_constant plotpdb poch pochrel poisson
syn keyword octaveForge  poisson_moments poisstat poly2ac poly2ar poly2mask
syn keyword octaveForge  poly2rc poly2sym poly2th poly_2_ex polyconf polystab
syn keyword octaveForge  poststd prctile premainder prestd prettyprint
syn keyword octaveForge  prettyprint_c primpoly princomp probably_prime
syn keyword octaveForge  proplan pserver psi psi_1_int psi_1piy psi_n
syn keyword octaveForge  pskdemod pskmod pulstran purelin pwelch pyulear
syn keyword octaveForge  qamdemod qammod qaskdeco qaskenco qfunc qfuncinv
syn keyword octaveForge  qp_kaiser qtable qtdecomp qtgetblk qtsetblk quad2dc
syn keyword octaveForge  quad2dcgen quad2dg quad2dggen quadc quadg quadndg
syn keyword octaveForge  quantile quantiz quotient rad2deg randdeintrlv
syn keyword octaveForge  randerr randint randintrlv random randsrc rankcorr
syn keyword octaveForge  ranks raylcdf raylinv raylpdf raylrnd raylstat rc2ac
syn keyword octaveForge  rc2ar rc2poly rceps read_options read_pdb rectpuls
syn keyword octaveForge  rectwin recv redef reduce redundancy reedmullerdec
syn keyword octaveForge  reedmullerenc reedmullergen regress relativeentropy
syn keyword octaveForge  release rem remainder remez removefilesemaphore
syn keyword octaveForge  renyi_entropy resample residued residuez reval
syn keyword octaveForge  rgb2gray ricedeco riceenco rledeco rleenco rmle
syn keyword octaveForge  rmoutlier rms roicolor rosenbrock rotate_scale
syn keyword octaveForge  rotparams rotv rsdec rsdecof rsenc rsencof rsgenpoly
syn keyword octaveForge  run2dtests samin samin_example saveMLPStruct
syn keyword octaveForge  save_vrml sawtooth sbispec scale_data scatterplot
syn keyword octaveForge  sclose scloseall scores second select_3D_points
syn keyword octaveForge  selmo selmo2 sem send server setfields
syn keyword octaveForge  setfilesemaphore sftrans sgolay sgolayfilt
syn keyword octaveForge  shannon_entropy shannonfanodeco shannonfanodict
syn keyword octaveForge  shannonfanoenco shanwavf sim sinc_gsl sinint
syn keyword octaveForge  sinvest1 skewness slurp_file socket sockets sos2tf
syn keyword octaveForge  sos2zp sound soundsc spearman specgram splot square
syn keyword octaveForge  squareform startmulticoremaster startmulticoreslave
syn keyword octaveForge  statistic std std2 stretchlim strjoin strsort strtoz
syn keyword octaveForge  strtrim subs sum_moments_nodes sumskipnan sumterms
syn keyword octaveForge  svds sym sym2poly symbols symerr symfsolve syminfo
syn keyword octaveForge  symlsolve sync synchrotron_1 synchrotron_2 syndtable
syn keyword octaveForge  systematize tabulate tansig tars taylorcoeff tcdf
syn keyword octaveForge  tclphoto tclsend tcoeff temp_name tempdir2
syn keyword octaveForge  test_d2_min_1 test_d2_min_2 test_d2_min_3
syn keyword octaveForge  test_ellipj test_fminunc_1 test_gsvd test_min_1
syn keyword octaveForge  test_min_2 test_min_3 test_min_4 test_minimize_1
syn keyword octaveForge  test_moving_surf test_ncrule test_nelder_mead_min_1
syn keyword octaveForge  test_nelder_mead_min_2 test_optiminterp
syn keyword octaveForge  test_optiminterp_mult test_quadg test_sncndn
syn keyword octaveForge  test_struct test_vmesh test_vrml_faces test_wpolyfit
syn keyword octaveForge  testfun testfun_octave textread tf2sos tfe
syn keyword octaveForge  tfestimate thfm tics tinv to_double today
syn keyword octaveForge  toggle_grace_use tpdf train transport_2 transport_3
syn keyword octaveForge  transport_4 transport_5 transpose trastd triang
syn keyword octaveForge  triangular tricontour trimean trimmean tripuls
syn keyword octaveForge  tsademo tstat tukeywin tunstallcode ucp uintlut
syn keyword octaveForge  unarydec unaryenc unidstat unifstat unitfeedback
syn keyword octaveForge  units unscale_parameters unvech upfirdn uppertri
syn keyword octaveForge  upsample use_sparse_jacobians var vec2mat vmesh vpa
syn keyword octaveForge  vrml_Background vrml_DirectionalLight
syn keyword octaveForge  vrml_PointLight vrml_ROUTE vrml_TimeSensor vrml_anim
syn keyword octaveForge  vrml_arrow vrml_browse vrml_cyl vrml_demo_tutorial_1
syn keyword octaveForge  vrml_demo_tutorial_2 vrml_demo_tutorial_3
syn keyword octaveForge  vrml_demo_tutorial_4 vrml_ellipsoid vrml_faces
syn keyword octaveForge  vrml_flatten vrml_frame vrml_group vrml_interp
syn keyword octaveForge  vrml_kill vrml_lines vrml_material vrml_newname
syn keyword octaveForge  vrml_parallelogram vrml_points vrml_select_points
syn keyword octaveForge  vrml_surf vrml_text vrml_thick_surf vrml_transfo
syn keyword octaveForge  waitbar wblstat welchwin wgn win32_MessageBox
syn keyword octaveForge  win32_ReadRegistry wpolyfit wpolyfitdemo write_pdb
syn keyword octaveForge  wsolve x25 x2mdate xcorr xcorr2 xcov xcovf xcubed
syn keyword octaveForge  xlsread xmlread xmlwrite xraylib xsquar y2res year
syn keyword octaveForge  yeardays zagzig zenity_calendar zenity_entry
syn keyword octaveForge  zenity_file_selection zenity_list zenity_message
syn keyword octaveForge  zenity_notification zenity_progress zenity_scale
syn keyword octaveForge  zenity_text_info zero_count zeta zeta_int zigzag
syn keyword octaveForge  zoom zp2sos zplane zscore

" Esto no sé lo que es
syn keyword octaveTodo  contained  TODO

" If you do not want these operators lit, uncommment them and the "hi link"
" below
syn match octaveLogicalOperator  "[&|~!]"

syn match octaveArithmeticOperator  "[-+]"
syn match octaveArithmeticOperator  "\.\=[*/\\^]"

syn match octaveRelationalOperator  "[=!~]="
syn match octaveRelationalOperator  "[<>]=\="

syn match octaveLineContinuation  "\.\{3}"
syn match octaveLineContinuation  "\\[ \t]*[#%]"me=e-1
syn match octaveLineContinuation  "\\[ \t]*$"

"syn match octaveIdentifier  "\<[a-zA-Z_][a-zA-Z0-9_]*\>"

" String
syn region octaveString  start=+'+ end=+'+	oneline
syn region octaveString  start=+"+ end=+"+	oneline

" If you don't like tabs
syn match octaveTab  "\t"

" Standard numbers
" Esta línea venía en el fichero original
"syn match octaveNumber  "\<\d\+[ij]\=\>"
" Esta es la que he puesto yo
syn match octaveNumber  "\<\d\+\([edED][-+]\=\d\+\)\=[ij]\=\>"

" floating point number, with dot, optional exponent
" Esta línea venía en el fichero original
"syn match octaveFloat      "\<\d\+\(\.\d*\)\=\([edED][-+]\=\d\+\)\=[ij]\=\>"
" Esta es la que he puesto yo
syn match octaveFloat  "\<\d\+\.\d*\([edED][-+]\=\d\+\)\=[ij]\=\>"

" floating point number, starting with a dot, optional exponent
syn match octaveFloat  "\.\d\+\([edED][-+]\=\d\+\)\=[ij]\=\>"

" Transpose character and delimiters: Either use just [...] or (...) aswell
"syn match octaveDelimiter  "[][]"
syn match octaveDelimiter  "[][()][{}]"

syn match octaveTransposeOperator  "[])a-zA-Z0-9.]'"lc=1

"syn match octaveSemicolon  ";"

syn match octaveComment  "[%#].*$"  contains=octaveTodo,octaveTab

syn match octaveError  "-\=\<\d\+\.\d\+\.[^*/\\^]"
syn match octaveError  "-\=\<\d\+\.\d\+[eEdD][-+]\=\d\+\.\([^*/\\^]\)"

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_octave_syntax_inits")
    if version < 508
        let did_octave_syntax_inits = 1
        command -nargs=+ HiLink hi link <args>
    else
        command -nargs=+ HiLink hi def link <args>
    endif

    "HiLink octaveBeginKeywords       Statement
    "HiLink octaveElseKeywords        Statement
    "HiLink octaveEndKeywords         Statement
    "HiLink octaveReserved            Statement
    HiLink octaveKeywords            Statement
    HiLink octaveCommands            Comando
    "HiLink octaveTransposeOperator  octaveOperator
    "HiLink octaveOperator           Operator
    "HiLink octaveLineContinuation    Special
    "HiLink octaveLabel			     Label
    "HiLink octaveConditional         Conditional
    "HiLink octaveRepeat              Repeat
    "HiLink octaveTodo                Todo
    HiLink octaveString              Texto
    "HiLink octaveDelimiter           Identifier
    "HiLink octaveTransposeOther      Identifier
    HiLink octaveNumber              Number
    HiLink octaveFloat               Float
    HiLink octaveFunction            Function
    HiLink octaveForge               Function
    "HiLink octaveError               Error
    "HiLink octaveImplicit            octaveStatement
    "HiLink octaveStatement           Statement
    "HiLink octaveSemicolon           SpecialChar
    HiLink octaveComment             Comment
    "HiLink octaveArithmeticOperator  octaveOperator
    "HiLink octaveRelationalOperator  octaveOperator
    "HiLink octaveLogicalOperator     octaveOperator
    "optional highlighting
    "HiLink octaveIdentifier         Identifier
    "HiLink octaveTab                Error

    delcommand HiLink
endif

let b:current_syntax = "octave"

"EOF	vim: ts=8 noet tw=100 sw=8 sts=0




"*****FIN DEL FICHERO*****
